﻿using Microsoft.Win32;
using CommunityToolkit.WinUI.Notifications;
using System.ServiceProcess;

namespace TORifyMe
{
    public class Torifyme : ApplicationContext
    {
        private readonly ServiceController sc;
        private readonly RegistryKey registry;
        private readonly NotifyIcon notifyIcon;
        private readonly ContextMenuStrip contextMenu1;
        private readonly ToolStripMenuItem StartStopMenuItem;
        private readonly ToolStripMenuItem ExitMenuItem;
        private readonly System.ComponentModel.IContainer components;

        private bool ServiceRunning = false;

        public Torifyme()
        {
            sc                  = new ServiceController { ServiceName = "tor" };
            registry            = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", true);

            ServiceRunning      = GetServiceStatus();

            components          = new System.ComponentModel.Container();
            contextMenu1        = new ContextMenuStrip();
            StartStopMenuItem   = new ToolStripMenuItem();
            ExitMenuItem        = new ToolStripMenuItem();

            contextMenu1.Items.AddRange(new ToolStripItem[] { StartStopMenuItem, ExitMenuItem });

            if (ServiceRunning)
            {
                StartStopMenuItem.Text = "Stop Tor";
            }
            else
            {
                StartStopMenuItem.Text = "Run Tor";
            }

            StartStopMenuItem.Click += new EventHandler(DoubleClickHandler);
            ExitMenuItem.Text = "Exit";
            ExitMenuItem.Click += new EventHandler(ExitProgram);

            notifyIcon = new NotifyIcon(components)
            {
                Icon = new Icon("tor_icon.ico"),
                ContextMenuStrip = contextMenu1,
                Text = "TORifyMe",
                Visible = true
            };

            notifyIcon.DoubleClick += new System.EventHandler(DoubleClickHandler);
        }

        private bool GetServiceStatus()
        {
            if (sc.Status == ServiceControllerStatus.Running || sc.Status == ServiceControllerStatus.StartPending)
            {
                return true;
            }

            return false;
        }

        private void SetManualProxy()
        {
            registry.SetValue("ProxyEnable", 1);
            registry.SetValue("ProxyServer", "socks=127.0.0.1:9050");
        }

        private void RemoveManualProxy()
        {
            registry.SetValue("ProxyEnable", 0);
        }

        private void StartService()
        {
            if (GetServiceStatus())
            {
                Console.WriteLine("Service is already running");
            }
            else
            {
                try
                {
                    Console.Write("Start pending... ");

                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 10));

                    if (sc.Status == ServiceControllerStatus.Running)
                    {
                        ServiceRunning = true;
                        Console.WriteLine("Service started successfully.");
                    }
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Could not start the service.");
                }
            }
        }

        private void StopService()
        {
            if (GetServiceStatus())
            {
                try
                {
                    Console.WriteLine("Stopping service...");

                    sc.Stop();
                    sc.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 10));

                    if (sc.Status == ServiceControllerStatus.Stopped)
                    {
                        ServiceRunning = false;
                        Console.WriteLine("Service stopped successfully.");
                    }
                } 
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Could not stop service.");
                }
            }
        }

        private void DoubleClickHandler(object Sender, EventArgs e)
        {
            string UserFeedback;

            if (!ServiceRunning)
            {
                StartService();
                SetManualProxy();
                StartStopMenuItem.Text = "Stop Tor";
                ServiceRunning = true;
                UserFeedback = "TOR-Mode started.";

            }
            else
            {
                StopService();
                RemoveManualProxy();
                StartStopMenuItem.Text = "Run Tor";
                ServiceRunning = false;
                UserFeedback = "TOR-Mode stopped.";
            }

            new ToastContentBuilder()
                .AddArgument("action", "viewConversation")
                .AddText(UserFeedback)
                .Show();
        }

        private void ExitProgram(object Sender, EventArgs e)
        {
            ExitThread();
        }
    }
}
