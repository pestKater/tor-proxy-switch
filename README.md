# Tor Proxy Switch

## System requirements

- Windows 10 Version 10.0.18362.0 or above
- .NET6-Framework (10.0.18362.0 or above) installed

## Getting started

First you have to download and unzip [Windows Expert Bundle](https://www.torproject.org/de/download/tor/). Rename the directory to for example "Tor" and put it in a desired location. 
(For Example C:\Tor). 
Open up CMD and navigate to Tor-Folder (for example cd C:\Tor\Tor) and run Tor.exe for confirmation if anything goes wrong. 

Then you can register Tor as a Service. (Tor.exe --install service)

After installing Tor, you can install this Tool. Prebuilt setup is located in the root of this repository.

## Usage

Start TORifyMe. An icon will be added to the system tray. You can double click the item to toggle Tor (alternative right-click -> Run/Stop Tor).
To Exit the tool, right-click and then click Exit.

## Authors and acknowledgment

Dominik Steuer

## Version history

### Version 1.0.2
- Changed user feedback to windows native toast messages
- Refactoring and tidying of code

### Version 1.0.0
- Initial release

## License
CC BY SA

Creative Commons Corporation (�Creative Commons�) is not a law firm and does not provide legal services or legal advice. Distribution of Creative Commons public licenses does not create a lawyer-client or other relationship. Creative Commons makes its licenses and related information available on an �as-is� basis. Creative Commons gives no warranties regarding its licenses, any material licensed under their terms and conditions, or any related information. Creative Commons disclaims all liability for damages resulting from their use to the fullest extent possible.